# The Caverns of Laghlyn

## Act 3

### The Road to Laghlyn

* Players must decide what to do with bodies
* Can loot bodies (*see below)
* Rest of night uneventful

#### The bandits have the following loot:
* 4x scimitars (1x broken, 2x damaged hit=1d6, 1x good condition)
* 4x light crossbows (2x broken, 1x badly damaged hit=1d8-1 15% chance of breaking each use, 1x good condition)
* 1x Bottle containing red liquid (identify = DC12 arcana, potion of healing 1d4 temp HP)
* 1x pouch containing a pipe and some fried leafy herb (1d4 temp HP) (**note, this can be used with the weed they bought in O.L. Sense of calm and connection w/ the world granting +1d4 to attack or saving throw, lasts 1 hour*)
* 1x translucent yellow stone (2)

The next morning you break camp. The day is considerably brighter than the one before and it seems like the bad weather has passed completely. You continue along the road for another mile or so before cresting a ridge, whereupon you catch your first sight of Laghlyn in the distance. Its tall towers and pointed spires glistening in the late morning sunlight.

You follow the road down from the ridge as it winds its way alongside the river toward the city. The volume of traffic increases steadily as you draw closer. Other small roads from the surrounding area join to this main road with farmers and traders from across the county and beyond travelling to the city to sell and trade their wares. A trader with a small cart tethered to a donkey approaches you from behind. He calls out a greeting.

* Interaction with trader from Old Laghlyn (Terryn)
* He knows of their exploits in the town and is happy to talk to them in person
* He will readily give advice on where to go and what to do in Laghlyn
* Some of this might not be the best advice
	- He will suggest staying in the old town if they want to save money (this is not good advice for anyone not from or intimately familiar with the old town)
	- He will recommend buying food from a restaurant down a lane off castle hill which will be closed and run down looking when the players get there
	- The other advice he gives will be sound although he doesn't know everything and will not be able to answer every question they have.

You bid the trader farewell and he continues on his way to the city. The road ahead follows the course of the river for a short distance before arriving at a bridge the other travellers on the road are using to cross. On the far side it continues in several directions, the largest of these leading towards the city. You follow this road through an ever increasing number of small shambling houses and buildings that line the way to the city walls. 

To the right of these, inside the city walls, you see a tall keep perched above the surrounding buildings. To the left are more houses and small shops selling groceries and other day-to-day items clustered along the river bank.

As you pass through the houses ahead you join the back of a short queue. The city guards are inspecting arriving carts at the gate.

* Interaction with guards
* They will ask questions but are ultimately not that interested in them as they are fairly unimpressive looking overall
* If they mention they are here for the mission they will be directed to see the mayor in the city hall
* The guards will be slightly more derisive in this case

### The City

You pass under an impressive stone arch with thick wooden doors and make your way into the city centre. The buildings within the walls are tall and narrow, often overhanging the street. The street in front of you is cobbled and well worn and just wide enough for a single carriage to pass.

* Describe Barrack St.
* Players can explore the city here

The facade of the city hall is a grand stately affair with tall columns and large doors of intricately carved wood, inset with panes of glass. Guards stand watch at the door and at each of the columns. You walk through these doors and enter a wide and ornately decorated room with many doors leading to further rooms and a broad staircase at the back leading to the upper levels. Between the doors are carved statues and impressive paintings. Clerks rush between rooms carrying stacks of paper and important looking documents while pages scurry behind them carrying trays filled with beakers and goblets. In front of you and to the right sits a stern looking dwarf woman.

* Interaction with the receptionist
* She is gruff and impolite but will do her job and will direct the players where to go.
* The mayor is in the meeting hall upstairs

The receptionist stands and beckons a guard to escort you to the meeting hall. Together you climb the polished stone staircase and walk out into an impressive hallway lined by carved wooden doors and more paintings of famous people and events from Leighann's past. The hallway is like the foyer below, bustling with clerks and scribes and pages rushing to and fro. You follow the guard and enter a large double doorway on the left of the hallway. The door is flanked by columns, carved with animals and filigrees and opens into a large room with a vaulted ceiling and more of the lavish decoration you've seen outside. Banners hang from the walls between the paintings and the wall ahead has a number of huge windows which look out onto the square below. At a table to your left a group of well dressed men are poring over a pile of documents and maps. The guard who is is accompanying you gestures for you to stop at the door and a young man, dressed in an explosion of ruffled clothes, approaches. The guard informs him of your business and the young man scurries over to the table to speak to one of the men. 