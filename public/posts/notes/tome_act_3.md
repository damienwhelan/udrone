# Recover the Tome
## Act 3

### Into the Forest

#### 54

The forest is difficult terrain to traverse. The ground is hilly and uneven and the trees are tightly packed together making it difficult for the taller members of the party to walk without getting hit or caught on branches.

The trail here is also much more difficult to follow. Little impression is left in the dry ground by either boots or feet.

* SURVIVAL CHECK (DC 15)
* SUCCEED GO TO 55, FAIL GO TO 56

#### 56

You wander for an hour or more before arriving at the bed of a small dried up stream which appears to have incredibly faint footprints running along it. You follow this through the trees as it twists its way round the small mounds of the forest floor. At the end of the stream bed you continue a little further only to find it has led you back to where you have started. 

* MAKE ANOTHER TRACKING? CHECK (DC 12)
* SUCCEED GO TO 55, FAIL RETRY CHECK

#### 55

Near the base of a large tree you see the faint impression of goblin tracks. You follow this trail for over an hour into the forest until you reach a point where the trees start to thin out a little and the forest floor becomes more lush with undergrowth.

In a clearing ahead stands an ancient mausoleum. It has long since been abandoned by whomever created it but there are very clear signs of recent use all around it.

The mausoleum us a tall, white, classical building with columns supporting an apex roof and carved stone walls with an opening leading to the top of a stairs.

* TO SURVEIL THE AREA GO TO 56A, TO PROCEED GO TO 57

#### 56A

You watch the outside of the mausoleum for any activity. After a short while there is a movement to the left of the building. A small patrol of goblins appears form the the trees on the far side of the clearing and walks in your direction.

* STEALTH CHECK (DC 14)
* SUCCEED GO TO 58, FAIL GO TO 59

#### 57

You cross the clearing toward the mausoleum and enter the doorway. On the steps, 5 feet below you, are two goblin lookouts.

* FIGHT LOOKOUTS W/ SURPRISE
* GO TO 63

#### 58

You all immediately crouch in the undergrowth to avoid being seen. The patrol continues in your direction but turns and walks across the clearing in front of the entrance to the mausoleum.

* TO ATTACK GO TO 60, TO AVOID GO TO 57

#### 59

You all immediately crouch in the undergrowth to avoid being seen. The patrol continues in your direction. The leader holds up a hand and barks an order to the patrol who all come to a halt in the middle of the clearing. He scans the treeline in front of him where you are now hiding. Between the trees are bushes, saplings and $\color{red}{fail\_char}$, standing almost completely upright, trying in vain to  hide behind a sapling which in absolutely no way provides them with anywhere near enough cover.

* GO TO 62

### Enter the mausoleum

#### 60

The goblins pass by the entrance to the mausoleum and continue on their patrol route unawares. When their back are turned you launch your attack.

* FIGHT GOBLIN PATROL W/ SURPRISE
* LOOKOUTS JOIN AFTER 2 ROUNDS
* GO TO 61

#### 61

With the goblins dispatched you make a quick check of the area before entering the mausoleum. Inside there is a staircase which descends into the gloom. At the bottom of the stairs you turn a corner and see a long corridor leading to a carved entrance and a room beyond.

* GO TO 61B

#### 62

The goblin leader spits a curse in your direction and orders the patrol to attack. 

* FIGHT THE GOBLIN PATROL
* AFTER 2 ROUNDS THE LOOKOUTS EMERGE TO JOIN IN
* GO TO 61

#### 63

The second goblin slumps to the ground, a faint gurgling noise leaving the side of his mouth. 

* CAN SEARCH BODIES HERE

You step over the bodies and turn the corner at the bottom of the stairs. A corridor leads to a carved stone entrance to a room beyond. In the centre of the room is a huge brazier, something which undoubtedly was used for offerings in the past. On the walls are carved reliefs depicting epic battles and great deeds committed by a group of holy knights. A foul smell hangs in the air of the room. Around the floor is scattered tattered clothing, rotting food and piles of rubbish left by the tomb's goblin occupiers. At the far and of the room is another carved entrance leading to a corridor.

* GO TO 64

#### 61B

You descend the stairs and turn the corner at the bottom. A corridor leads to a carved stone entrance to a room beyond. In the centre of the room is a huge brazier, something which undoubtedly was used for offerings in the past. On the walls are carved reliefs depicting epic battles and great deeds committed by a group of holy knights. A foul smell hangs in the air of the room. Around the floor is scattered tattered clothing, rotting food and piles of rubbish left by the tomb's goblin occupiers. At the far and of the room is another carved entrance leading to a corridor.

* GO TO 65

#### 64

Suddenly you hear a noise approaching from the far corridor. A squad of goblins appears through the doorway and charges you.

* FIGHT THE GOBLIN SQUAD
* AFTER TWO ROUNDS THE PATROL ARRIVES

From the entrance you hear a goblin sergeant shout a curse and you turn to see another squad of goblins entering from the corridor behind.

* FINISH THE GOBLIN FIGHT

The last goblin falls dead at your feet. The room is now littered with bodies. 

* SEARCH ROOM (OPTIONAL) (NOTHING USEFUL HERE)
* GO TO 66

#### 65

Suddenly you hear a noise approaching from the far corridor. A squad of goblins appears through the doorway and charges you.

* FIGHT THE GOBLIN SQUAD

The last goblin falls dead, slumped in a heap against the huge brazier.

* SEARCH ROOM (OPTIONAL) (NOTHING USEFUL HERE)
* GO TO 66

#### 66

You leave the room by the far doorway and enter another dark corridor.

* PRESSURE PLATE TRAP (FIRST NON HALFLING CHAR)

As $\color{red}{unlucky\_char}$ steps forward there is a moment where the floor beneath their foot feels like it is falling away. Under the floor and in the walls there is a dull grinding sound. A huge, rusted blade suddenly swings from the shadows in the ceiling.

* ACROBATICS CHECK (DC 15)
* SUCCEED GO TO 67, FAIL GO TO 68

#### 68

You raise your $\color{red}{weapon / item}$ just in time to stop the blade from striking your head but the blade is only deflected slightly and carves a deep gouge in your arm and shoulder.

* LOSE A 3 HP
* GO TO 69

#### 67

You duck skilfully under the swinging blade and watch it slam into the wall beside you, peppering the air with fragments of stone.

* GO TO 69

#### 69

You round the corner and see another carved entrance to a room at the end of the corridor. Inside the room you can see yet more goblins in the process of laying an ambush for you. They are being directed by an impressive, mean looking hobgoblin, who is barking orders at them. You must attack them now if you are to stand any chance of victory.

* FIGHT HOBGOBLIN AND RETINUE
* GO TO 70

#### 70

With the last of the enemy now dead the mausoleum falls silent. In this room you see more of the stone relief carvings along the walls. In the centre of the far wall, facing the entrance, is the statue of a knight. He is holding his shield to one side and with his other hand is stabbing his sword through some mysterious, hellish creature.

* SEARCH OF THE ROOM UNCOVERS THE FOLLOWING

	- THE TOME!!!
	- A small, engraved gold cup (worth 25 gold)
	- A small bag of gold and silver pieces (10x gold, 16x silver)
	- Several shovels & other farming equipment
	- Some peasants clothing & belongings
	
* ROLL INVESTIGATION (DC 14)
* SUCCEED GO TO 71, FAIL GO TO 75

### Bonus round

#### 71

At the top of the statues base, between the knights feet and hidden by the writhing creature is a small panel with several curious symbols carved in it. Below them is a second row of similar symbols which can be depressed. It is a puzzle!

By selecting the correct symbol from the bottom row to complete the sequence of the top row you will solve the puzzle.

* SUCCEED GO TO 73, FAIL GO TO 72

#### 72

There is a grinding noise followed by a sudden sound of breaking stone. This was not the correct choice. The puzzle's secrets will remain lost forever.

* GO TO 75

#### 73

There is a grinding sound as part of the wall behind the statue slides away to reveal a hidden room. 

This room has sat, completely untouched for centuries. Along either wall to the side lies a row of sarcophagi. Each with a reposed figure of a knight in armour on top. 

You begin moving into the room when suddenly you hear another scraping sound. You turn, and to your horror, you see the lids of all of the sarcophagi moving to open. A bony had emerges from one and grips the edge to pull himself up. $\color{red}{Some\ number\ of}$ partially armoured skeletal warriors emerge from the coffins.

* FIGHT THE SKELETONS
* GO TO 74

#### 74

The last skeleton crumples and turns to dust and his armour falls to a heap on the ground. The tomb now falls silent again.

* SEARCH OF THE ROOM UNCOVERS THE FOLLOWING

	- A Rod of Rulership (Paddy)
	- A Ring of Protection (Dudley)
	- A Wand of Fireballs (Emmett)
	- More gold (162 pieces)
	- A bag of jewels (worth ~200 gold)
	- Several jewel encrusted, gold pieces of jewellery (worth ~150 gold)
	- Adamantine Armour (Phil)
	- Elven Chain (Jimi)
	- A Robe of Eyes (Aileen)
* GO TO 75

#### 75

Congratulations! You have defeated the goblin scourge and bested the knights of the silent star. 

Great fortune and adventure await you in your next quest in the Barony of Udrone.
