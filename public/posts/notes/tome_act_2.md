# Recover the Tome
## Act 2

### The road to Rosse

#### 15

The road out of town is surrounded on both sides by small fields filled with an array of growing crops and grazing animals. Several fields are being worked by local farmers and their helpers, some of which call out friendly greetings as you pass, others of which seem completely indifferent to your presence. 

* INTERACTION WITH FARMERS IF WANTED (PLAYER INITIATED)

The road is long but well maintained and progress is easy. Fields filled with yellow wheat and barley, rippling in the breeze, long grass being cut short by bleating sheep and occasional trees which dot the landscape pass by continuously for the first few hours of travel.

* GO TO 16

#### 16

Shortly after the second hour you discover a strange house to the side of the road. Set in a small copse of trees and bushes the house itself is a shambling, leaning, conical affair made of grey stone, with a narrow, winding staircase wrapped around its outside toward a door in the upper level. From the back of the building protrudes a crooked chimney with a pointed cover on top. The yard around the house is empty and the house itself seems to be otherwise currently unoccupied. 

* SHOW IMAGE OF HOUSE
* TO ENTER THE YARD GO TO 17, TO CONTINUE ON THE ROAD GO TO 36

#### 17

In the yard there are fresh tracks and signs of recent use. There is a small, covered wood store to one side of the yard with an axe resting against one of the posts and a large stump, used for chopping wood nearby. The winding stairs has several broken and uneven steps but is otherwise ok and leads to a door in the upper part of the house. The door is locked.

* ABILITY CHECK (SMASH DOOR, PICK LOCK, USE MAGIC, ETC.) (DC 15)
* IF THE DOOR IS DAMAGED GO TO 18, IF NOT GO TO 23, TO RETURN TO THE ROAD GO TO 45

#### 18

The door crashes open sending splinters of the door frame flying through the air inside. Beyond the door is a small room with walls which curve inward toward the ceiling. Narrow shafts of light pierce the gloom through the shuttered windows. In the middle of the room is an armchair and a small side table with several books stacked upon it. Under the window, opposite the door, is a large table piled high with books and manuscripts. To the left of the table is a narrow wooden stairs leading to the floor below. Opposite the stairs is a small fireplace with a trammel hook and several pots stacked on the floor beside. Between the fireplace and the door is a large basin with a small amount of grey-brown liquid in the bottom. (\* The documents in this room are interesting but useless. They can be kept if wanted.)

* GO TO 19

#### 19

The room downstairs is large than the upper room but is considerably less spacious. Every wall is filled with shelves heaving with books, documents, jars of strange liquid and a bewildering array of objects and curios. Around the floor are wooden crates filled with more of the same. Near the wall opposite the stairs is a wooden bed with blankets thrown across it. Adjacent to that, against the wall, midway between the bed and the stairs is a wooden chest with thick brass bands.

* TO SEARCH THE CHEST GO TO 20, TO LEAVE IT AND SEARCH THE REST OF THE ROOM GO TO 21

#### 20

You lift the lid of the chest and lean it back against the wall. Inside there appears to be a mix of clothing and other bric-a-brac. You are lifting back the top layer when suddenly you all hear the sound of men talking and horses in the yard outside.

* GO TO 22

#### 21

You begin searching the room for interesting or useful items. After a moment $\color{red}{Keth}$ motions for everyone to stop. They hold a finger to their lips and slowly make their way toward the shuttered window. You can hear the sound of horses approaching.

* GO TO 22


#### 22

A patrol of the Baron's cavalry have stopped to check on the house having spotted the broken door from the road. The sergeant of the patrol dismounts and approaches the winding stairs.
 
* THE PLAYERS CANNOT FIGHT. IT IS SUICIDE OR IMPRISONMENT. THEY CAN ESCAPE BY LEAPING THROUGH THE WINDOW AND RUNNING THROUGH THE TREES TO THE FIELDS BEYOND.
* GO TO 28A

#### 23

You hear the faint click as the lock's mechanism disengages and the door swings open. Beyond the door is a small room with walls which curve inward toward the ceiling. Narrow shafts of light pierce the gloom through the shuttered windows. In the middle of the room is an armchair and a small side table with several books stacked upon it. Under the window, opposite the door, is a large table piled high with books and manuscripts. To the left of the table is a narrow wooden stairs leading to the floor below. Opposite the stairs is a small fireplace with a trammel hook and several pots stacked on the floor beside. Between the fireplace and the door is a large basin with a small amount of grey-brown liquid in the bottom. (\* The documents in this room are interesting but useless. They can be kept if wanted.)

* GO TO 24

#### 24 

The room downstairs is large than the upper room but is considerably less spacious. Every wall is filled with shelves heaving with books, documents, jars of strange liquid and a bewildering array of objects and curios. Around the floor are wooden crates filled with more of the same. Near the wall opposite the stairs is a wooden bed with blankets thrown across it. Adjacent to that, against the wall, midway between the bed and the stairs is a wooden chest with thick brass bands.

* TO SEARCH THE CHEST GO TO 25, TO LEAVE IT AND SEARCH THE REST OF THE ROOM GO TO 26

#### 25

You lift the lid of the chest and lean it back against the wall. Inside there appears to be a mix of clothing and other bric-a-brac. You are lifting back the top layer when suddenly you all hear the sound of horses on the road outside.

* GO TO 27

#### 26

You begin searching the room for interesting or useful items. After a moment $\color{red}{Keth}$ motions for everyone to stop. They hold a finger to their lips and slowly make their way toward the shuttered window. You can hear the sound of horses approaching.

* GO TO 27

#### 27

You freeze immediately. Through a crack in the shuttered window you can see a group of lightly armoured men on horseback. Emblazoned on their shields and the pennant they fly is the coat of arms of Baron Halberg. As the patrol pass the yard several of the men look in towards the house.

* TO ESCAPE THE BUILDING TURN TO 28, TO HIDE TURN TO 29

#### 28 

At the back of the house is a window which is partially obscured under the stairs outside. You gently open the shutter and one by one lower yourselves as quietly as possible to the ground below. 

* STEALTH ROLL (EACH PLAYER) (DC 12?)
* SUCCEED GO TO 30, FAIL GO TO 31

#### 29

Everyone in the party holds perfectly still, hoping to avoid detection.

* ROLL STEALTH CHECK (DC 12?)
* SUCCEED GO TO 32, FAIL GO TO 33

#### 30

Silently you cross the yard out of sight of the patrol and enter the copse of trees behind the house. Two of the patrol remain mounted at the gate while the rest search the yard. After several minutes the sergeant emerges from the house and motions to his men. They immediately abandon the search and remount their horses. The sergeant then joins them and the patrol resumes its journey toward Old Laghlyn.

Once a suitable amount of time has passed you emerge from the trees.

* GO TO 44

#### 31

You are crossing the yard when one of the patrolmen cries out. Immediately all of the others turn to see you. 

You sprint toward the copse of trees at the back of the property.

* GO TO 34

#### 32

One of the patrolmen stops suddenly at the gate. He reaches down and adjusts something on his saddle before moving off to rejoin the rest of the patrol down the road.

* GO TO 44

#### 33

$\color{red}{char\_who\_fails}$ turns to move toward the stairs but as they make their way through the boxes littering the floor their clothing snags on a large metal dish perched on top of one of these boxes. The dish falls to the floor with a loud clang and raises a small cloud of dust around where it landed. The entire party collectively holds its breath.

* GO TO 43

#### 34

You stumble between the trees, tripping over fallen branches and tangled roots. The shouts of the patrolmen can be heard in the yard as they try to organise themselves to stop you. The sergeant re-emerges from the top of the house and issues a gruff command to his men. You push on deeper into the forest ignoring the sharp branches pulling at your hair and clothing and scratching your skin and eventually emerge into the field beyond. 

The field is filled with long green stalks of corn, about 3-4 feet high. By crouching and moving carefully you can evade the baron's men. They will be unable to follow you through the trees and the field beyond is surrounded by a thick bramble. The only entrance, located several minutes away, further down the road. 

If you are lucky you can make it to the next field and the small gully which runs along its edge. This small drainage channel runs almost parallel to the road and will provide you a means to evade your pursuers long enough for them to abandon their hunt.

* ROLL STEALTH CHECK (DC 12?)
* GO TO 45

#### 35

You turn away from the door and make your way back down the winding stairs, across the yard and back out onto the road.

* GO TO 36

#### 36

You press on with your journey. After a short time $\color{red}{Keth}$ spots something in the distance.

* ROLL PERCEPTION CHECK (DC 12)
* SUCCEED GO TO 37, FAIL GO TO 38

#### 37

In the distance is a group of lightly armoured men on horseback. Emblazoned on their shields and the pennant they fly is the coat of arms of Baron Halberg.

* CHOOSE TO STAY AND TALK OR HIDE
* TO STAY GO TO 39, TO HIDE GO TO 40

#### 38

In the distance is a group of armoured men on horseback.

* CHOOSE TO STAY AND TALK OR HIDE
* TO STAY GO TO 41, TO HIDE GO TO 42

#### 40

The patrol approaches your hiding place. One of the patrolmen stops suddenly, just feet from where $\color{red}{PaddYeQuest}$ is crouched. 

The patrolman reaches down to adjust something on his saddle before rejoining the rest of the patrol and continuing on his way.

* GO TO 45

#### 39

As the patrol approaches the sergeant gives a signal to his men who slow to a halt and surround you.

* INTERACTION WITH SERGEANT. (HAS ISSUE W/ WOOD ELVES BUT WILL EVENTUALLY LET THE PLAYERS GO.)
* GO TO 45

#### 41

As the patrol approaches you notice the coat of arms of Baron Halberg emblazoned on the men's shields. The sergeant gives a signal to his men who slow to a halt and surround you.

* INTERACTION WITH SERGEANT. (HAS ISSUE W/ WOOD ELVES BUT WILL EVENTUALLY LET THE PLAYERS GO.)
* GO TO 45

#### 42

As the patrol approaches your hiding place you notice the coat of arms of Baron Halberg emblazoned on the men's shields. One of the patrolmen stops suddenly, just feet from where $\color{red}{PaddYeQuest}$ is crouched. 

The patrolman reaches down to adjust something on his saddle before rejoining the rest of the patrol and continuing on his way.

* GO TO 45

#### 43

Through the crack in the shutters the yard and road outside appear empty. Suddenly a patrolman appears around the gate and enters the yard. He is followed by the rest of the patrol.

* GO TO 43a

#### 43a

You kick open the shutter of a window at the rear of the house and leap into the yard below. Sprinting across the hardened ground you plunge headlong into the trees at the back of the property. 

* GO TO 34

#### 44

Once you are satisfied that the Baron's patrol have left you resume your search of the property. Unfortunately there is little of value here except some interesting manuscripts and a small golden statuette of unknown origin.

* GO TO 45

### The trail to the forest

#### 45

You make your way back to the road and continue on your journey toward Rosse. It is late in the afternoon and the sun is lowering in the sky. The fields around you are becoming steadily more wild  with each passing mile. After a while $\color{red}{Raznar}$ stops and examines the area. 

"This is the spot", he says and gestures across the fields to the right of the road. There is a gap in the hedgerow and a trail which leads off across the field. You all pass through the gap in the hedge and begin your trek cross country.

* GO TO 46

#### 46

The terrain is hilly and overgrown, but the narrow trail is clear and shows signs of having been recently used. Despite this,the going is tough and progress is considerably slower than when you were on the road. You need to be constantly aware for potholes and dips, brambles and other plants which have grown across the track.

* ACROBATICS CHECK FOR 3 RANDOM CHARS (DC 12?)
* SUCCEED: NO EFFECT, FAIL: LOSE 1 HP
* GO TO 47

#### 47

You press on, following the trail over small hills and mounds, leaping across ditches and gullies. The sun is now particularly low in the sky creating long shadows amongst the bumpy terrain. At one particularly low point in the track, between two mounds you hear a rustling sound.

* ANIMAL ATTACK (BOAR?)
* AFTER 2 ROUNDS IF THE BOAR IS STILL ALIVE IT WILL FLEE
* IF BOAR DEAD GO TO 48, IF BOAR ALIVE GO TO 49

#### 48

With a final shudder the boar collapses on the ground. 

* GO TO 50

#### 49

The boar is simply looking for an easy meal. It has not found one here and so it turns and flees through the undergrowth.

* GO TO 50

#### 50

Over the next hill you catch sight of the edge of the forest. The first few trees are nearby but the forest grows more and more dense the further you look. 

* IT SHOULD TAKE ~30-40 MINS TO REACH EDGE OF THE FOREST
* IF CAMP & BOAR DEAD GO TO 51, IF CAMP & NOT DEAD GO TO 52
* IF NOT CAMP GO TO 53

#### 51

You make your camp in a sheltered spot near the edge of the forest. You build a small fire, eat a much needed meal and settle down for the night.

* ROLL D20. 1-10: UNEVENTFUL NIGHT, 11-20: GOBLIN PATROL ATTACK
* GO TO 54

#### 52

You make your camp in a sheltered spot near the edge of the forest. You build a small fire, eat a much needed meal and settle down for the night.

* BOAR ATTACK (WEAKENED)
* ROLL D20. 1-16: UNEVENTFUL NIGHT, 17-20: GOBLIN PATROL ATTACK
* GO TO 54

#### 53

The forest is now incredibly dark. It is almost impossible to see where you are going. The forest floor is treacherous under foot with the constant danger of tripping and low hanging branches.

* ACROBATICS CHECKS FOR ALL PLAYERS. (DC 16) SUCCEED: NO EFFECT, FAIL: LOSE 1 HP
* IF CONTINUE GO TO 54, IF CAMP & BOAR DEAD GO TO 51, IF CAMP & NOT DEAD GO TO 52