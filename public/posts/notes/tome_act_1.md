# Recover the Tome
## Act 1

### Arrival at the town

#### 1

A thin cloud scuds across the azure summer sky. In the square below the sun is glinting off the bright burnished breastplate of $\color{red}{Jorf\ the\ fighter}$. On the notice board in front of them a small scrap of paper is gently fluttering in the summer breeze. $\color{red}{Jorf}$ raises a gloved hand and pulls the notice from its pin

* THE NOTE IS READ ALOUD BY THE $\color{red}{JORF}$

$\color{red}{Jorf}$ looks slowly around the bustling square and sets off, past the Town Hall towards Centaur Street and the tavern, through the milling crowd of locals going about their daily business.

* GO TO 2

#### 2

In a narrow alley between two houses, $\color{red}{PaddYeQuest}$ & $\color{red}{Keth\ Rekomash}$ stand, observing the tavern. Crumpled in $\color{red}{PaddYeQuest}$’s hand is the same notice pleading for assistance. For two days the pair have watched the comings and goings at the tavern with no obvious change to the everyday routine. 
$\color{red}{Keth}$ stretches wearily and turns to $\color{red}{the\ halfling}$, gesturing toward the other end of the alley, hinting that it might be time to leave. $\color{red}{PaddYeQuest}$ suddenly sticks out a hand. 
From across the square a figure is approaching the tavern. A tall fighter, standing completely apart from the rest of the ordinary townsfolk strides forward purposefully, stopping at the tavern door. The two quickly duck behind the crates and other rubbish in the alley to avoid being seen as the $\color{red}{Jorf}$ checks the area before entering.

* GO TO 3

#### 3

Inside, the taproom is busy with locals and traders from nearby towns. There are seats lining the adjacent wall to the left and under the stairs beside the entrance with rectangular tables and long bench seats in front of them. There are more tables and chairs in the middle of the floor and a large hearth in the middle of the right-hand wall. At the rear of the taproom is the bar. Standing behind it is the stout figure of the innkeeper, a hill dwarf with a stocky frame, bald head and thick brown moustache, framed on all sides by the huge casks of beer and other drinks available here.

At the table nearest the hearth sits a wizened old man, with a long, pointed hat, gently puffing on a pipe which he occasionally uses to scratch various parts of his head and face. He has a rather pensive look about him but as soon as he notices the $\color{red}{Jorf}$ approach the table his mood brightens considerably.

* INTERACTION BETWEEN $\color{red}{RAZNAR}$ AND $\color{red}{JORF}$
* GO TO 4

#### 4

As $\color{red}{Jorf}$ takes a seat at the table the door of the tavern opens again and $\color{red}{PaddYeQuest}$ and $\color{red}{Keth}$ make their way quietly into the taproom and sit at one of the seats against the wall, opposite the hearth, observing the $\color{red}{Jorf}$ and $\color{red}{Raznar}$, occasionally whispering something between them. After several minutes the pair leave their seats, approach the wizard’s table and introduce themselves.

* INTERACTION BETWEEN $\color{red}{RAZNAR}$, $\color{red}{JORF}$, $\color{red}{PADDYEQUEST}$ AND $\color{red}{KETH}$
* GO TO 5

#### 5

The two take their seats at the table and begin listening to the wizard’s story. From across the room comes a deep, booming roar.
“Are you going to buy anything or just sit there taking up space that could go to some useful customers?”
“Arseholes!”, he says, half under his breath but loud enough for the party to hear.

* INTERACTION WITH THE INNKEEPER

(He will allow them to stay in the tavern if they buy at least 3 drinks between the 4 of them, otherwise they will have to take their business outside.)

* IF INSIDE GO TO 6, IF OUTSIDE GO TO 9

#### 6

As the party are returning to their discussion $\color{red}{Keth}$ leans in to the wizard and whispers something in his ear. He nods slowly and with his staff, pushes out a chair at the far side of the table. Then, without turning his head, he gestures towards the shadows in the upstairs landing, beckoning something to join the table.

* GO TO 7

#### 7

After a moment $\color{red}{Thia\ Naïlo}$ vaults over the railing and lands loudly on the wooden floor, kicking up a huge plume of dust in the process. This loud noise startles the innkeeper who unleashes a stream of obscenities in the direction of $\color{red}{the\ elf}$.

* GO TO 8

#### 8

* INTERACTION WITH THE INNKEEPER
($\color{red}{Thia\ Naïlo}$ must now buy a drink)

$\color{red}{Thia\ Naïlo}$ joins the table with the rest of the party. The innkeeper roughly slams the drink down on the tabletop, spilling a large amount of it in the process but not seeming to notice or care. He makes his way back to the bar muttering angrily to himself all the way.

“Fucking arsehole pricks. Why in the fuck….etc.”

The party all turn toward the wizard who has watched this entire scene with a faint smile of amusement on his face.

“As I was saying…”

* $\color{red}{RAZNAR}$ OUTLINES THE JOB IN HAND
* GO TO 12

#### 9

As the party are returning to their discussion $\color{red}{Keth}$ leans in to the wizard and whispers something in his ear. He nods slowly and taps his staff gently on the ground. Then, without turning his head, he gestures towards the upstairs window of a nearby empty building, beckoning something to join the group.

* GO TO 10

#### 10

After a moment $\color{red}{Thia\ Naïlo}$ vaults over the window ledge and lands loudly on the flag stoned pavement, kicking up a huge plume of dust in the process. This loud noise startles a nearby cow who begins pulling wildly at its rope in an effort to escape. The farmer eventually brings the cow under control and continues his way toward the town square but not before unleashing a stream of obscenities in the direction of $\color{red}{the\ elf}$.

* GO TO 11

#### 11

A hand appears on $\color{red}{Thia}$’s shoulder. It is the town guard, who’s attention has been drawn by the loud noises from his morning routine.

* INTERACTION WITH THE TOWN GUARD
($\color{red}{Thia}$ must now pay small fee for disturbing the ~guard’s rest~ peace)

$\color{red}{Thia\ Naïlo}$ joins the rest of the party. The town guard smiles, pocketing the coin and goes back to his morning routine, walking down the street, cordially greeting the local townsfolk.

The party all turn toward the wizard who has watched this entire scene with a faint smile of amusement on his face.

“As I was saying…”

* $\color{red}{RAZNAR}$ OUTLINES THE JOB IN HAND

* GO TO 12

#### 12

\* The party agrees to meet back in the taproom of the tavern the next morning.

\* Everyone is free to explore the town for the rest of the day. You can buy or otherwise obtain any materials you might feel are necessary.

\* You must find somewhere to stay tonight. You can sleep rough but this will increase the chance of being attacked.

* GO TO 13

### Leaving the town

#### 13

It is another bright and clear morning in Western Udrone, with the exception of the taproom at the Stonehaven tavern. Inside it is dark and dusty and filled with the smell of stale beer and some of the tavern's more hygienically challenged patrons. Even at this early hour there are several propping up the bar or sitting sullenly at the tables, far apart from each other. One by one the members of the party arrive to sit at the table near the hearth.

* ANY FINAL PREPARATIONS MUST BE MADE NOW BEFORE LEAVING TOWN

* GO TO 14

#### 14

You leave the tavern and make your way across the bustling square. At the opposite corner, beside the grocer's shop lies the entrance to Green Lane, which passes between a row of houses before turning south into Rosse Street. The sun is shining, birds are singing the townsfolk are happily going about their business and there is a gentle breeze at your backs. Good omens for fair travel.