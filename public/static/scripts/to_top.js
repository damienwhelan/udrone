	// Set a function onscroll - this will activate if the user scrolls
window.onscroll = function() {
    // Set the height to check for
  var appear = 20
  if (window.pageYOffset >= appear) {
    // If more show the element
    document.getElementById("toTop").style.opacity = '1'
    document.getElementById("toTop").style.pointerEvents = 'all'
  } else {
    // Else hide it
    document.getElementById("toTop").style.opacity = '0'
    document.getElementById("toTop").style.pointerEvents = 'none'
  }
}
